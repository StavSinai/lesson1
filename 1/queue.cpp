#include "queue.h"
#include <iostream>


void initQueue(queue* q, unsigned int size)
{
	q->num = new int[size];
	q->front = size - 1;	// size -1 cause can so the first index in enqueue will be size%size which equals to 0
	q->back = size - 1;
	q->maxSize = size;
}

void enqueue(queue* q, unsigned int newValue)
{
	q->back = (q->back + 1) % q->maxSize;
	q->num[q->back] = newValue;
}

int dequeue(queue* q)
{
	q->front = (q->front + 1) % q->maxSize;
	if (q->front == q->back)	// when full returns -1
	{
		return -1;
	}
	return q->num[q->front];
}

void cleanQueue(queue* q)
{
	delete(q);
}