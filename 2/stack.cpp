#include "stack.h"

void push(stack* s, unsigned int element)
{
	stack* temp = new stack();
	temp->num = element;
	temp->link = s;
	s = temp;
}

int pop(stack* s)
{
	stack* temp = s;
	s = s->link;
	delete(temp);
}

void initStack(stack* s)
{
	s->num = 0;
	s->link = nullptr;
}

void cleanStack(stack* s)
{
	delete(s);
}