#ifndef LINKEDLIST_H
#define LINKEDLIST_H


typedef struct node
{
	int num;
	struct node* next;
}node;

void insertAtEnd(node** head, node* newNode);
int deleteNode(node** head, int num);

#endif 
