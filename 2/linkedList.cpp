#include "linkedList.h"

void insertAtEnd(node** head, node* newNode)
{
	node* curr = *head;
	if (!*head) // empty list!
	{
		*head = newNode;
	}
	else
	{
		while (curr->next) // while the next is NOT NULL (when next is NULL - that is the last node)
		{
			curr = curr->next;
		}

		curr->next = newNode;
		newNode->next = 0;
	}
}

int deleteNode(node** head)
{
	node* head = new node();
	node* temp = (*head);
	node* newNode = new node();
	node* curr = (*head);
	int i = 0;

	while (temp->next)
	{
		newNode->num = i;
		newNode->next = nullptr;

		temp->next = newNode;
		temp = newNode;
	}

	delete(head);

	while (head != nullptr)
	{
		*head = (*head)->next;
	}

	return 0;
}
